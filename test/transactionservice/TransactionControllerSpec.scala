package transactionservice

import javax.inject.{Inject, Singleton}

import akka.stream.Materializer
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import transactionservice.TransactionEntities.{Transaction, TransactionBody}

import scala.collection.concurrent.TrieMap

@Singleton
class TransactionRepositoryFake @Inject()(e: TransactionExecutionContext) extends TransactionRepositoryImpl()(e) {
  transactionSource ++= TrieMap(
    3l -> TransactionBody(500, "cars", None),
    4l -> TransactionBody(200, "type", None),
    5l -> TransactionBody(200, "type", Some(3)),
    6l -> TransactionBody(67, "banks", Some(1)),
    7l -> TransactionBody(1200, "", Some(10)),
    9l -> TransactionBody(150, "", Some(10)),
    10l -> TransactionBody(100, "", Some(9))
  )
}

class TransactionControllerSpec extends PlaySpec with Results with GuiceOneAppPerSuite with MockitoSugar {

  implicit lazy val materializer: Materializer = app.materializer

  private val guiceApplicationBuilder = new GuiceApplicationBuilder()
    .overrides(bind[TransactionRepository].to[TransactionRepositoryFake].in[Singleton])

  private val transactionController = guiceApplicationBuilder.injector().instanceOf[TransactionController]

  "Transaction Controller" should {

    "return" in {
      val result = call(transactionController.getById("3"), FakeRequest())
      status(result) mustEqual 200
    }

    "return status ok for adding new transaction with no parent_id" in {
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "amount": 5000, "type":"cars" }"""))
      val result = call(transactionController.process("1"), request)
      status(result) mustEqual 200
      contentAsString(result) mustEqual "{\"status\":\"ok\"}"
    }

    "return status ok for adding new transaction with parent_id" in {
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "amount": 5000, "type":"cars", "parent_id":10 }"""))
      val result = call(transactionController.process("2"), request)
      status(result) mustEqual 200
      contentAsString(result) mustEqual "{\"status\":\"ok\"}"
    }

    "return status 400 for adding new transaction with not valid json body" in {
      val request = FakeRequest(POST, "/").withJsonBody(Json.parse("""{ "a": 5000, "type":"cars" }"""))
      val result = call(transactionController.process("10"), request)
      status(result) mustEqual 400
    }

    "return transaction for existing transaction_id with empty parent_id" in {
      val expectedJson = Transaction(3, 500, "cars", None).toJson.toString()
      val result = call(transactionController.getById("3"), FakeRequest())
      status(result) mustEqual 200
      contentAsString(result) mustEqual expectedJson
    }

    "return transaction for existing transaction_id with parent_id" in {
      val expectedJson = Transaction(6, 67, "banks", Some(1)).toJson.toString()
      val result = call(transactionController.getById("6"), FakeRequest())
      status(result) mustEqual 200
      contentAsString(result) mustEqual expectedJson
    }

    "return status 404 for not existing transaction_id" in {
      val result = call(transactionController.getById("999"), FakeRequest())
      status(result) mustEqual 404
    }

    "return status 400 for not valid transaction_id" in {
      val resultForNonPositiveId = call(transactionController.getById("-1"), FakeRequest())
      status(resultForNonPositiveId) mustEqual 400

      val resultForNonNumberId = call(transactionController.getById("abc"), FakeRequest())
      status(resultForNonNumberId) mustEqual 400
    }

    "return status empty list for getting transaction with not existing type" in {
      val result = call(transactionController.getByType("unknown"), FakeRequest())
      status(result) mustEqual 200
      contentAsString(result) mustEqual "[]"
    }

    "return status list with ids for getting transactions with existing type" in {
      val result = call(transactionController.getByType("type"), FakeRequest())
      status(result) mustEqual 200
      contentAsString(result) mustEqual "[4, 5]"
    }

    "return sum for getting transactions with existing transaction_id" in {
      val result = call(transactionController.sum("7"), FakeRequest())
      status(result) mustEqual 200
      contentAsString(result) mustEqual "{\"sum\":1200}"
    }

    "return sum 0 for getting transactions with not existing parent_id" in {
      val result = call(transactionController.sum("999"), FakeRequest())
      status(result) mustEqual 200
      contentAsString(result) mustEqual "{\"sum\":0}"
    }

    "return sum 0 for getting transactions with not valid parent_id" in {
      val result = call(transactionController.sum("yxz"), FakeRequest())
      status(result) mustEqual 400
    }

  }
}
