package transactionservice

import org.scalatest._
import play.api.test.Helpers._
import transactionservice.TransactionEntities.{Sum, Transaction}

import scala.concurrent.Future

class TransactionRepositoryImplSpec extends AsyncWordSpec {

  private val transactionRepositoryImpl = baseApplicationBuilder.injector().instanceOf[TransactionRepository]

  "Transaction Repository" should {

    "eventually add new transaction to repository and return it by id" in {
      val trans = Transaction(id = 1, amount = 5.0, tType = "cars", parent_id = None)
      transactionRepositoryImpl.create(trans).flatMap { id =>
        transactionRepositoryImpl.getById(id).map { t =>
          assert(t.contains(trans))
        }
      }
    }

    "eventually update transaction if transaction_id is already exist" in {
      val trans = Transaction(id = 1, amount = 5.0, tType = "unicType", parent_id = None)
      val transNewOne = Transaction(id = 1, amount = 55.0, tType = "NewCars", parent_id = Some(25))
      transactionRepositoryImpl.create(trans).flatMap { _ =>
        transactionRepositoryImpl.create(transNewOne).flatMap { _ =>
          transactionRepositoryImpl.getById(1).map { t =>
            assert(t.contains(transNewOne))
          }
        }
      }
    }

    "eventually return None for not existing transaction_id" in {
      val nonexistentId = 999
      transactionRepositoryImpl.getById(nonexistentId).map { t =>
        assert(t.isEmpty)
      }

    }

    "eventually return list of transaction_id by type" in {
      val tType = "cars"
      val futuresForAddingNewTransaction = List(
        transactionRepositoryImpl.create(Transaction(id = 2, amount = 5.0, tType = tType, parent_id = None)),
        transactionRepositoryImpl.create(Transaction(id = 3, amount = 15.0, tType = tType, parent_id = None)),
        transactionRepositoryImpl.create(Transaction(id = 4, amount = 15.0, tType = "another", parent_id = None))
      )

      val futureSeq = Future.sequence(futuresForAddingNewTransaction)
      futureSeq.flatMap { _ =>
        transactionRepositoryImpl.getWithTypes(tType).flatMap { t =>
          assert(t == List(2, 3))
        }
      }
    }

    "eventually return empty list for not existing type" in {
      val nonexistentType = "nonexistingType"
      transactionRepositoryImpl.getWithTypes(nonexistentType).flatMap { t =>
        assert(t == List.empty)
      }
    }

    "eventually return sum for transaction amount for parent_id" in {
      val parentId = 5
      val amountArray = Array(5.0, 15.0, 20.0)
      val futuresForAddingNewTransaction = List(
        transactionRepositoryImpl.create(Transaction(id = parentId, amount = amountArray(0), tType = "", parent_id = None)),
        transactionRepositoryImpl.create(Transaction(id = 6, amount = amountArray(1), tType = "", parent_id = Some(parentId))),
        transactionRepositoryImpl.create(Transaction(id = 7, amount = amountArray(2), tType = "", parent_id = Some(parentId))),
        transactionRepositoryImpl.create(Transaction(id = 8, amount = 15.0, tType = "another", parent_id = Some(6)))
      )

      val futureSeq = Future.sequence(futuresForAddingNewTransaction)
      futureSeq.flatMap { _ =>
        transactionRepositoryImpl.sum(parentId).flatMap { t =>
          assert(t == Sum(amountArray.sum))
        }
      }
    }

    "eventually return sum for parent_id even if there is no transaction_id = parent_id" in {
      val parentId = 9
      val futuresForAddingNewTransaction = List(
        transactionRepositoryImpl.create(Transaction(id = 10, amount = 15.0, tType = "another", parent_id = Some(9)))
      )

      val futureSeq = Future.sequence(futuresForAddingNewTransaction)
      futureSeq.flatMap { _ =>
        transactionRepositoryImpl.sum(parentId).flatMap { t =>
          assert(t == Sum(15.0))
        }
      }
    }

    "eventually return sum 0 for not existing parent_id" in {
      val nonexistentId = 11
      transactionRepositoryImpl.sum(nonexistentId).flatMap { t =>
        assert(t == Sum(0.00))
      }
    }

  }
}
