import sbt.Keys._

name := "transaction-service-ruban"

version := "0.1"

scalaVersion := "2.12.4"

val jodaConvert =  "org.joda" % "joda-convert" % "1.9.2"
val scalaGuice = "net.codingwell" %% "scala-guice" % "4.1.1"
val scalatestplusPlay = "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2"
val mockito =  "org.mockito" % "mockito-core" % "2.13.0"

libraryDependencies ++= Seq(guice, scalaGuice, jodaConvert, scalatestplusPlay, mockito)

lazy val root = (project in file(".")).enablePlugins(PlayJava)