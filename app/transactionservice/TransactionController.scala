package transactionservice

import javax.inject.Inject

import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc.{Action, _}
import transactionservice.TransactionEntities.{Transaction, TransactionBody}
import play.api.data.format.Formats._

import StringHelper.StringRich

import scala.concurrent.{ExecutionContext, Future}

object StringHelper {

  implicit class StringRich(s: String) {
    def toLongOption: Option[Long] = try {
      val l = s.toLong
      if (l > 0) Some(l) else None
    } catch {
      case _: NumberFormatException => None
    }
  }

}

class TransactionController @Inject()(cc: TransactionControllerComponents)(implicit ec: ExecutionContext)
  extends TransactionBaseController(cc) {

  private val form: Form[TransactionBody] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "amount" -> of(doubleFormat),
        "type" -> text,
        "parent_id" -> optional(longNumber)
      )(TransactionBody.apply)(TransactionBody.unapply)
    )
  }

  def process(transactionId: String): Action[AnyContent] = transactionAction.async { implicit request =>
    transactionId.toLongOption.fold(Future.successful(BadRequest.asInstanceOf[Result])) { id =>
      processJsonPost(id)
    }
  }

  private def processJsonPost[A](transaction_id: Long)(implicit request: TransactionRequest[A]): Future[Result] = {
    def failure(badForm: Form[TransactionBody]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: TransactionBody) = {
      transactionResourceHandler
        .create(Transaction(transaction_id, input.amount, input.tType, input.parent_id))
        .map { _ => Ok(Json.obj("status" -> "ok")) }
    }

    form.bindFromRequest().fold(failure, success)
  }

  def getById(transactionId: String): Action[AnyContent] = transactionAction.async { implicit request =>
    transactionId.toLongOption.fold(Future.successful(BadRequest.asInstanceOf[Result])) { id =>
      transactionResourceHandler.getById(id).map {
        case Some(t) => Ok(t.toJson)
        case None => NotFound
      }
    }
  }

  def getByType(tType: String): Action[AnyContent] = transactionAction.async { implicit request =>
    transactionResourceHandler.getWithTypes(tType).map { tList =>
      val response = tList.mkString("[", ", ", "]")
      Ok(response)
    }
  }

  def sum(transactionId: String): Action[AnyContent] = transactionAction.async { implicit request =>
    transactionId.toLongOption.fold(Future.successful(BadRequest.asInstanceOf[Result])) { id =>
      transactionResourceHandler.sum(id).map { sum =>
        Ok(Json.toJson(sum))
      }
    }
  }

}