package transactionservice

import javax.inject.Inject
import play.api.libs.json._
import transactionservice.TransactionEntities.{Sum, Transaction}
import scala.concurrent.{ExecutionContext, Future}

object TransactionEntities {

  case class TransactionBody(amount: Double, tType: String, parent_id: Option[Long])

  case class Sum(sum: Double)

  case class Transaction(id: Long, amount: Double, tType: String, parent_id: Option[Long]) {
    def this(id: Long, body: TransactionBody) = this(id, body.amount, body.tType, body.parent_id)
    def body: TransactionBody = TransactionBody(amount, tType, parent_id)
    def toJson: JsValue = Json.toJson(this)
  }

  implicit val transactionBodyWrites: Writes[TransactionBody] = Writes { t =>
    Json.obj("amount" -> t.amount, "type" -> t.tType, "parent_id" -> Some(t.parent_id))
  }

  implicit val transactionWrites: Writes[Transaction] = Writes { t =>
    Json.obj("id" -> t.id.toString, "amount" -> t.amount, "type" -> t.tType, "parent_id" -> Some(t.parent_id))
  }

  implicit val sumWrites: Writes[Sum] = Writes { t => Json.obj("sum" -> t.sum) }

}

/**
  * Controls access to the backend data
  */
class TransactionResourceHandler @Inject()(transactionRepository: TransactionRepository)(implicit ec: ExecutionContext) {

  def create(input: Transaction): Future[Long] = {
    transactionRepository.create(input)
  }

  def getWithTypes(tType: String): Future[List[Long]] = {
    transactionRepository.getWithTypes(tType)
  }

  def getById(id: Long): Future[Option[Transaction]] = {
    transactionRepository.getById(id)
  }

  def sum(id: Long): Future[Sum] = {
    transactionRepository.sum(id)
  }

}