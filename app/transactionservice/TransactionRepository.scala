package transactionservice

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import transactionservice.TransactionEntities.{Sum, Transaction, TransactionBody}

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

trait TransactionRepository {

  def create(transaction: Transaction): Future[Long]

  def getById(transaction: Long): Future[Option[Transaction]]

  def getWithTypes(tType: String): Future[List[Long]]

  def sum(id: Long): Future[Sum]

}

class TransactionExecutionContext @Inject()(actorSystem: ActorSystem)
  extends CustomExecutionContext(actorSystem, "transactionRepository.dispatcher")

/**
  * A custom execution context is used here to establish that blocking operations should be
  * executed in a different thread than Play's ExecutionContext, which is used for CPU bound tasks.
  */
@Singleton
class TransactionRepositoryImpl @Inject()()(implicit ec: TransactionExecutionContext ) extends TransactionRepository {

  protected val transactionSource: TrieMap[Long, TransactionBody] = TrieMap[Long, TransactionBody]()

  override def create(transaction: Transaction): Future[Long] = {
    Future {
      transactionSource += (transaction.id -> transaction.body)
      transaction.id
    }
  }

  override def getWithTypes(tType: String): Future[List[Long]] = {
    Future {
      transactionSource.filter(_._2.tType == tType).keys.toList.sorted
    }
  }

  override def sum(transactionId: Long): Future[Sum] = {
    Future {
      Sum(transactionSource
        .filter { case (id, body) => id == transactionId || body.parent_id.contains(transactionId)}
        .values
        .map(_.amount)
        .sum
      )
    }
  }

  override def getById(transactionId: Long): Future[Option[Transaction]] = {
    Future {
      transactionSource.filterKeys(_ == transactionId)
        .headOption
        .map { case (id, body) => new Transaction(id, body)}
    }
  }

}