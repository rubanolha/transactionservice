package transactionservice

import javax.inject.Inject

import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._

class TransactionRouter @Inject()(controller: TransactionController) extends SimpleRouter {

  val prefix = "/transactionservice"

  override def routes: Routes = {
    case POST(p"/transaction/$transaction_id") => controller.process(transaction_id)
    case GET(p"/transaction/$transaction_id")  => controller.getById(transaction_id)
    case GET(p"/types/$tType")                 => controller.getByType(tType)
    case GET(p"/sum/$transaction_id")          => controller.sum(transaction_id)

  }
}
