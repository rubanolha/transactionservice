package transactionservice

import javax.inject.Inject

import play.api.http.{FileMimeTypes, HttpVerbs}
import play.api.i18n.{Langs, MessagesApi}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

trait TransactionRequestHeader extends MessagesRequestHeader with PreferredMessagesProvider

class TransactionRequest[A](
  request: Request[A],
  val messagesApi: MessagesApi
) extends WrappedRequest(request)
  with TransactionRequestHeader

class TransactionActionBuilder @Inject()(messagesApi: MessagesApi, playBodyParsers: PlayBodyParsers)(
  implicit val executionContext: ExecutionContext)
  extends ActionBuilder[TransactionRequest, AnyContent]
    with HttpVerbs {

  val parser: BodyParser[AnyContent] = playBodyParsers.anyContent

  type TransactionRequestBlock[A] = TransactionRequest[A] => Future[Result]

  override def invokeBlock[A](request: Request[A], block: TransactionRequestBlock[A]): Future[Result] = {

    val future = block(new TransactionRequest(request, messagesApi))

    type TransactionRequestBlock[A] = TransactionRequest[A] => Future[Result]

    future.map { result =>
      request.method match {
        case GET | HEAD =>
          result.withHeaders("Cache-Control" -> s"max-age: 100")
        case _ => result
      }
    }
  }
}

/**
  * Packages up the component dependencies for the Transaction controller
  * to minimize the surface area exposed to the controller, so the
  * controller only has to have one thing injected.
  */
case class TransactionControllerComponents @Inject()(transactionActionBuilder: TransactionActionBuilder,
  transactionResourceHandler: TransactionResourceHandler,
  actionBuilder: DefaultActionBuilder,
  parsers: PlayBodyParsers,
  messagesApi: MessagesApi,
  langs: Langs,
  fileMimeTypes: FileMimeTypes,
  executionContext: scala.concurrent.ExecutionContext)
  extends ControllerComponents

/**
  * Exposes actions and handler to the Transaction Controller by wiring the injected state into the base class.
  */
class TransactionBaseController @Inject()(pcc: TransactionControllerComponents) extends BaseController {
  override protected def controllerComponents: ControllerComponents = pcc

  def transactionAction: TransactionActionBuilder = pcc.transactionActionBuilder

  def transactionResourceHandler: TransactionResourceHandler = pcc.transactionResourceHandler
}
