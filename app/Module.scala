import javax.inject._

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment}
import transactionservice.{TransactionRepository, TransactionRepositoryImpl}

/**
  * Sets up custom components for Play.
  */
class Module(environment: Environment, configuration: Configuration) extends AbstractModule with ScalaModule {

  override def configure(): Unit = {
    bind[TransactionRepository].to[TransactionRepositoryImpl].in[Singleton]
  }

}
