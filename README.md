This is an example of basic RESTful web service that stores some transactions(in memory) and returns information about those transactions. The transactions to be stored have a type and an amount. The service supports returning all transactions of a type. Also, transactions can be linked to each other (using a ”parent id”), so the service can return the total amount involved for all transactions linked to a particular transaction.

Api Spec
`PUT /transactionservice/transaction/$transaction_id`

Body:
  ` { "amount":double,"type":string,"parent_id":long }` 
where:

• transaction id is a long specifying a new transaction

• amount is a double specifying the amount

• type is a string specifying a type of the transaction.

• parent id is an optional long that may specify the parent transaction of this transaction


`GET /transactionservice/transaction/$transaction_id Returns: { "amount":double,"type":string,"parent_id":long }`

`GET /transactionservice/types/$type`

Returns: [long, long, ... ]

A json list of all transaction ids that share the same type $type.

`GET /transactionservice/sum/$transaction_id`

Returns: { "sum": double }

A sum of all transactions that are transitively linked by their parent_id to $transaction_id.